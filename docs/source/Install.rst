.. Basilisk documentation master file, created by
   sphinx-quickstart on Mon Sep 23 13:52:19 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _bskInstall:

Install
=======

.. toctree::
   :maxdepth: 1
   :caption: Contents:
   
   Install/installOnLinux
   Install/installOnMacOS
   Install/installOnWindows
   Install/pullCloneBSK
   Install/InstallWithCmakeOptions
   Install/installOptionalPackages
   Install/installPowerUserTips
   Install/customPython




