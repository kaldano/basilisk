.. toctree::
   :hidden:


.. _pullCloneBSK:


Pulling/Cloning a copy from the Repository
==========================================

The Basilisk frameworked is developed using the Git version control system.  The following directions explain how to cloe or pull a copy from the repository.  

#. If needed, create your own `bitbucket.org <http://bitbucket.org>`__ account

#. Use a browser to go to the `Basilisk Bitbucket
   Repository <https://bitbucket.org/avslab/basilisk>`__

#. Click on the Clone button on this page, and select the ``https``
   option instead of the ssh option

#. Copy the project url (omit ``git clone``) from the bitbucket clone panel
   
   .. image:: ../_images/static/bitbucket-clone-panel.png
      :align: center
      :scale: 40%
		
#. Clone into preferred Git client (Source Tree for instance), or just clone the repository in the directory containing Basilisk. In SourceTree, use ``clone from url``, add the Basilisk repository url (without ``.git`` on the end), and select ``develop`` branch to pull the latest code.
   
   .. image:: ../_images/static/sourcetree-clone-panel.png
      :align: center
      :scale: 40%


