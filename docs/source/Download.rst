
.. _bskDownload:

Download Source
===============

.. image:: _images/static/Basilisk-Logo.png
       :align: right
       :scale: 50 %

The Basilisk software framework source code is open hosted on `Bitbucket.org <https://Bitbucket.org>`_.
Go to https://bitbucket.org/avslab/basilisk, log in with your Bitbucket account and download or :ref:`fork <makeBskFork>` a copy of
the Basilisk code.  As this is the raw source code, you need to next :ref:`install <bskInstall>` and compile the code before you can run it.
