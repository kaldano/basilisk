
Welcome to the Basilisk Astrodynamics Framework Documentation
=============================================================

Within these web pages you will find information about Basilisk and the related simulation software. The pages contain information on how to install the software framework, read up on known issues or the latest release notes, as well as find common questions answered.  Note that information can also be found in the Basilisk forum.

Note that the file structure in the Documentation section mirrors that of the Basilisk source folder.  This makes it simple to navigate and find information on a particular Basilisk module.  The modules contain access to the module documentation as well as the module unit tests. The examples folder provides a series of documented example script to try.  Note that the example script web page provides a direct link to view the associated sample code.  Similarly, the Basilisk module documentation contains links to view the module test documentation and a direct link to view the code.

.. toctree::
   :maxdepth: 1
   :caption: Basilisk:
   
   About
   Download
   Install
   Quick-Start
   Support
   Documentation/index
   Forum

.. toctree::
   :maxdepth: 1
   :caption: Vizard:

   Vizard/Vizard
   Vizard/VizardDownload
   Vizard/VizardReleaseNotes
   Vizard/VizardGUI
   Vizard/vizardSettings





