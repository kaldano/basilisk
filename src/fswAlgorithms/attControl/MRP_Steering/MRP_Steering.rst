
This module implements a kinematic MRP steering control whose command output is a desired body rate vector.

The module
:download:`PDF Description </../../src/fswAlgorithms/attControl/MRP_Steering/_Documentation/AVS-Sim-MRP_Steering-2016-0108.pdf>`
contains further information on this module's function, how to run it, as well as testing.
