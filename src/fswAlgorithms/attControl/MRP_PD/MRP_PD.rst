
Attitude tracking control module using proportional/derivative MRP feedback and no RWs.

This module is similar to :ref:`MRP_Feedback`, but without the RW or the integral feedback option. The feedback control is able to asymptotically track a reference attitude if there are no unknown dynamics and the attitude control torque is implemented with a thruster set.   The module
:download:`PDF Description </../../src/fswAlgorithms/attControl/MRP_PD/_Documentation/Basilisk-MRP_PD-2019-03-29.pdf>`
contains further information on this module's function, how to run it, as well as testing.
