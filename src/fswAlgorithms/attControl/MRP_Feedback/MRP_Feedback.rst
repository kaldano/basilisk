
This module implements a nonlinear MRP feedback control that tracks
an arbitrary reference orientation.

The module
:download:`PDF Description </../../src/fswAlgorithms/attControl/MRP_Feedback/_Documentation/Basilisk-MRP_Feedback-2016-0108.pdf>`
contains further information on this module's function,
how to run it, as well as testing.

