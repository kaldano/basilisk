
The eclipse class gets generates sun illumination state at a particular inertial position.

The module
:download:`PDF Description </../../src/simulation/environment/eclipse/_Documentation/Basilisk-eclipse-20171101.pdf>`
contains further information on this module's function,
how to run it, as well as testing.

