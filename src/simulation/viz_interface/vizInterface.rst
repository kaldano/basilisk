
This module provides an interface to package up Basilisk messages and pass them onto the :ref:`Vizard <vizard>` application.



